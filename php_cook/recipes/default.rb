application = "php"
deploy = node[:deploy][application]

config_dir = "/etc/apache2/sites-available"


template "#{deploy[:deploy_to]}/current/php_app/index.php" do
    source "php.erb"
    mode 0755
    group 'www-data'
    owner 'www-data'
    variables(:properties => deploy[:variables])
    only_if do
      File.directory?("#{deploy[:deploy_to]}/current/")
    end
end

template "#{config_dir}/php.conf" do
    source "php_conf.erb"
    mode 0755
    group 'www-data'
    owner 'www-data'
    variables(:properties => deploy[:variables])
    only_if do
      File.directory?("#{config_dir}")
    end
end
